from django.conf import settings

def expose_wafer_talks(request):
    return {'WAFER_TALKS_OPEN': settings.WAFER_TALKS_OPEN}
